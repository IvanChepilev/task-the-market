package com.themarket.demo.services;

import com.themarket.demo.models.Contract;
import com.themarket.demo.models.DTOs.ContractClosingDTO;
import com.themarket.demo.repositories.contracts.ContractRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class ContractServiceTests {

    @Mock
    ContractRepository mockRepository;

    @InjectMocks
    ContractServiceImpl service;

    @Test
    public void getAll_Should_CallRepository() {
        service.getAll();
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_CallRepository() {
        service.getById(1);
        Mockito.verify(mockRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void getAllActiveContracts_Should_CallRepository() {
        service.getAllActiveContracts();
        Mockito.verify(mockRepository, Mockito.times(1)).getAllActiveContracts();
    }
    @Test
    public void getBySellerId_Should_CallRepository() {
        service.getBySellerId(1);
        Mockito.verify(mockRepository, Mockito.times(1)).getBySellerId(1);
    }

    @Test
    public void getByItemId_Should_CallRepository() {
        service.getByItemId(1);
        Mockito.verify(mockRepository, Mockito.times(1)).getByItemId(1);
    }
}
