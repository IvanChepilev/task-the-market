package com.themarket.demo.utils.mappers;

import com.themarket.demo.models.Contract;
import com.themarket.demo.models.DTOs.ContractDTO;
import com.themarket.demo.models.Item;
import com.themarket.demo.models.Status;
import com.themarket.demo.services.contracts.ContractService;
import org.springframework.stereotype.Component;

@Component
public class ContractMapper {


    public Contract fromDTO(Contract contract, ContractDTO contractDTO) {
        contract.setPrice(contractDTO.getPrice());
        return contract;
    }

    public Contract fromDTO(Item item, ContractDTO contractDTO) {
        Contract contract = new Contract();
        contract.setSeller(item.getOwner());
        contract.setItemId(item);
        contract.setPrice(contractDTO.getPrice());
        Status status = new Status();
        status.setId(1);
        status.setStatusName("true");
        contract.setStatus(status);
        return contract;
    }
}
