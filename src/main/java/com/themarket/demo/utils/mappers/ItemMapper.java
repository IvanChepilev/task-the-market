package com.themarket.demo.utils.mappers;

import com.themarket.demo.models.DTOs.ItemDTO;
import com.themarket.demo.models.Item;
import com.themarket.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemMapper {

    private final UserService userService;

    @Autowired
    public ItemMapper(UserService userService) {
        this.userService = userService;
    }


    public Item fromDTO (ItemDTO itemDTO){
        Item item = new Item();
        dtoToObject(itemDTO, item);
        return item;
    }

    private void dtoToObject(ItemDTO itemDTO, Item item) {
        item.setName(itemDTO.getName());
        item.setOwner(userService.getById(itemDTO.getOwnerId()));
    }
}
