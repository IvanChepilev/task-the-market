package com.themarket.demo.repositories;

import com.themarket.demo.exeptions.EntityNotFoundException;
import com.themarket.demo.models.Contract;
import com.themarket.demo.models.DTOs.ContractClosingDTO;
import com.themarket.demo.repositories.contracts.ContractRepository;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ContractRepositoryImpl implements ContractRepository {

    private final SessionFactory sessionFactory;

    public ContractRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Contract create(Contract contract) {
        try (Session session = sessionFactory.openSession()) {
            session.save(contract);
        }
        return contract;
    }

    @Override
    public Contract update(Contract contract) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(contract);
            session.getTransaction().commit();
        }
        return contract;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Contract getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contract> query = session.createQuery(
                    "from Contract where id = :id", Contract.class);
            query.setParameter("id", id);
            List<Contract> list = query.list();
            return list.get(0);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Contract", id);
        }
    }

    @Override
    public Contract getByItemId(int itemId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contract> query = session.createQuery(
                    "from Contract where itemId.id = :itemId", Contract.class);
            query.setParameter("itemId", itemId);
            List<Contract> list = query.list();
            return list.get(0);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Contract", itemId);
        }
    }

    @Override
    public List<Contract> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contract> query = session.createQuery("from Contract", Contract.class);
            return query.list();
        }
    }

    @Override
    public List<Contract> getAllActiveContracts() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contract> query = session.createQuery("from Contract where status.statusName = :name", Contract.class);
            query.setParameter("name", "true");
            return query.list();
        }
    }

    @Override
    public List<Contract> search(Optional<String> parameter, Optional<Integer> id) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder();

            switch (parameter.get()) {
                case "itemId":
                    queryString.append("from Contract where itemId.id = :id and status.id = 2 ");
                    break;
                case "sellerId": {
                    queryString.append("from Contract where seller.id = :id and status.id = 2");
                    break;
                }
                case "buyerId": {
                    queryString.append("from Contract where buyer.id = :id and status.id = 2");
                    break;
                }
            }
            Query<Contract> query = session.createQuery(queryString.toString(), Contract.class);
            query.setParameter("id", id.get());
            return query.list();
        }
    }

    @Override
    public List<Contract> getBySellerId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contract> query = session.createQuery(
                    "from Contract where seller.id = :sellerId", Contract.class);
            query.setParameter("sellerId", id);
            List<Contract> list = query.list();
            return list;
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Contract", id);
        }
    }
}
