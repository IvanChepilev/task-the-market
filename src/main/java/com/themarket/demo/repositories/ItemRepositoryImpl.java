package com.themarket.demo.repositories;

import com.themarket.demo.exeptions.EntityNotFoundException;
import com.themarket.demo.models.Item;
import com.themarket.demo.models.User;
import com.themarket.demo.repositories.contracts.ItemRepository;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ItemRepositoryImpl implements ItemRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ItemRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Item> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Item> query = session.createQuery("from Item ", Item.class);
            return query.list();
        }
    }

    @Override
    public Item getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Item> query = session.createQuery("from Item where id = :id", Item.class);
            query.setParameter("id", id);
            List<Item> list = query.list();
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Item", id);
            }
            return list.get(0);
        }
    }

    @Override
    public List<Item> getByOwnerId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Item> query = session.createQuery(
                    "from Item where owner.id = :id", Item.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public void create(Item item) {
        try (Session session = sessionFactory.openSession()) {
            session.save(item);
        }
    }

    @Override
    public void update(Item item) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(item);
            session.getTransaction().commit();
        }
    }

    @Override
    public Item getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Item> query = session.createQuery(
                    "from Item where name = :name", Item.class);
            query.setParameter("name", name);
            List<Item> items = query.list();
            if (items.size() == 0) {
                throw new EntityNotFoundException("Item", "name", name);
            }
            return items.get(0);
        }
    }
}
