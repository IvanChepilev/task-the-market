package com.themarket.demo.repositories;

import com.themarket.demo.exeptions.EntityNotFoundException;
import com.themarket.demo.models.User;
import com.themarket.demo.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserRepositoryImpl  implements UserRepository {

    private final SessionFactory sessionFactory;

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Query<User> query = session.createQuery(
                    "from User where id = :id", User.class);
            query.setParameter("id", id);
            List<User> list = query.list();
            return list.get(0);
        }catch (Exception e){
            throw new EntityNotFoundException("User", id);
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }
}
