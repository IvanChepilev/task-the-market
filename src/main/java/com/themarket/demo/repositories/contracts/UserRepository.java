package com.themarket.demo.repositories.contracts;

import com.themarket.demo.models.User;

public interface UserRepository {

    User getById(int id);

    void update(User user);
}
