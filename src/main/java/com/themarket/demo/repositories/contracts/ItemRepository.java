package com.themarket.demo.repositories.contracts;

import com.themarket.demo.models.Item;
import com.themarket.demo.models.User;

import java.util.List;

public interface ItemRepository {

    List<Item> getAll();

    Item getById(int id);

    List<Item> getByOwnerId(int id);

    void create(Item item);

    void update(Item item);

    Item getByName (String name);
}
