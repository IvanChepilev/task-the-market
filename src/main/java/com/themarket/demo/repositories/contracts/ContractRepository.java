package com.themarket.demo.repositories.contracts;

import com.themarket.demo.models.Contract;
import com.themarket.demo.models.DTOs.ContractClosingDTO;

import java.util.List;
import java.util.Optional;

public interface ContractRepository {

    Contract create(Contract contract);

    Contract update(Contract contract);

    void delete(int id);

    Contract getById(int id);

    Contract getByItemId(int id);

    List<Contract> getAll();

    List<Contract> getAllActiveContracts();

    List<Contract> search(Optional<String> parameter, Optional<Integer> id);

    List<Contract> getBySellerId(int id);
}
