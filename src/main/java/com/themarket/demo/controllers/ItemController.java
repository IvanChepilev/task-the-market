package com.themarket.demo.controllers;

import com.themarket.demo.exeptions.DuplicateEntityException;
import com.themarket.demo.exeptions.EntityNotFoundException;
import com.themarket.demo.models.DTOs.ItemDTO;
import com.themarket.demo.models.Item;
import com.themarket.demo.services.contracts.ItemService;
import com.themarket.demo.utils.mappers.ItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/items")
public class ItemController {

    private final ItemService service;
    private final ItemMapper mapper;

    @Autowired
    public ItemController(ItemService service, ItemMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Item> getAll(){
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Item getById (@PathVariable int id){
        try {
            return service.getById(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/user/{id}")
    public List<Item> getItemsByOwner(@PathVariable int id){
       return service.getByOwnerId(id);
    }

    @PostMapping
    public Item create(@Valid @RequestBody ItemDTO itemDto){
          try {
              Item item = mapper.fromDTO(itemDto);
              service.create(item);
              return item;
          }catch (DuplicateEntityException e){
              throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
          }catch (EntityNotFoundException e) {
              throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
          }
    }

}
