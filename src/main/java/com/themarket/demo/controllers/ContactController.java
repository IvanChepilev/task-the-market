package com.themarket.demo.controllers;

import com.themarket.demo.exeptions.EntityNotFoundException;
import com.themarket.demo.models.Contract;
import com.themarket.demo.models.DTOs.ContractClosingDTO;
import com.themarket.demo.models.DTOs.ContractDTO;
import com.themarket.demo.services.contracts.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contracts")
public class ContactController {

    private final ContractService service;

    @Autowired
    public ContactController(ContractService service) {
        this.service = service;
    }

    @GetMapping
    public List<Contract> getAll(@RequestParam(required = false) Optional<String> parameter,
                                 @RequestParam(required = false) Optional<Integer> id) {
        return service.search(parameter, id);
    }

    @GetMapping("/true")
    public List<Contract> getAllActiveContacts() {
        return service.getAllActiveContracts();
    }

    @GetMapping("/{id}")
    public Contract getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/seller/{id}")
   public List<Contract> getAllContractsBySellerId(@PathVariable int id){
        try {
            return service.getBySellerId(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Contract create(@Valid @RequestBody ContractDTO contractDTO) {
        try {
            return service.create(contractDTO);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping
    public Contract update(@RequestBody ContractDTO contractDTO) {
        try {
            return service.update(contractDTO);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/closing")
    public void closing(@RequestBody ContractClosingDTO contractClosingDTO) {
        try {
            service.closing(contractClosingDTO);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
