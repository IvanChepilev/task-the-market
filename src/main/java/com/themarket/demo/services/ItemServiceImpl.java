package com.themarket.demo.services;

import com.themarket.demo.exeptions.DuplicateEntityException;
import com.themarket.demo.exeptions.EntityNotFoundException;
import com.themarket.demo.models.Item;
import com.themarket.demo.models.User;
import com.themarket.demo.repositories.contracts.ItemRepository;
import com.themarket.demo.services.contracts.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    private final ItemRepository repository;

    @Autowired
    public ItemServiceImpl(ItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Item> getAll() {
        return repository.getAll();
    }

    @Override
    public Item getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Item> getByOwnerId(int id) {
        return repository.getByOwnerId(id);
    }

    @Override
    public void create(Item item) {
        boolean duplicateExists = true;

        try {
            Item existingItem = repository.getByName(item.getName());
            if (existingItem.getId() == item.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Item", "name", item.getName());
        }
        repository.create(item);
    }

    @Override
    public void update(Item item) {
        repository.update(item);
    }

    @Override
    public Item setItemOwner(Item item, User owner) {
        Item itemToSetOwner = new Item();
        itemToSetOwner.setId(item.getId());
        itemToSetOwner.setName(item.getName());
        itemToSetOwner.setOwner(owner);
        return itemToSetOwner;
    }
}
