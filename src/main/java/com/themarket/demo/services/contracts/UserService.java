package com.themarket.demo.services.contracts;

import com.themarket.demo.models.Contract;
import com.themarket.demo.models.User;

public interface UserService {

    User getById(int id);

    void update(User user);

    User setSellerAccount(User seller, Contract contract);

    User setBuyerAccount(User buyer, Contract contract);

}
