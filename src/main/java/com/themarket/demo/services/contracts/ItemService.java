package com.themarket.demo.services.contracts;

import com.themarket.demo.models.Item;
import com.themarket.demo.models.User;

import java.util.List;

public interface ItemService {

    List<Item> getAll();

    Item getById(int id);

    List<Item> getByOwnerId(int id);

    void create(Item item);

    void update(Item item);

    Item setItemOwner(Item item, User owner);
}
