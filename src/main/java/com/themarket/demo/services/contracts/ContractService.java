package com.themarket.demo.services.contracts;

import com.themarket.demo.models.Contract;
import com.themarket.demo.models.DTOs.ContractClosingDTO;
import com.themarket.demo.models.DTOs.ContractDTO;
import com.themarket.demo.models.User;

import java.util.List;
import java.util.Optional;

public interface ContractService {

    Contract create(ContractDTO contractDTO);

    Contract update(ContractDTO contractDTO);

    void delete(int id);

    Contract getById(int id);

    Contract getByItemId(int id);

    List<Contract> getAll();

    List<Contract> getAllActiveContracts();

    void closing(ContractClosingDTO contractClosingDTO);

    Contract setContractForClosing(Contract contract, User user);

    List<Contract> search(Optional<String> parameter, Optional<Integer> id);

    List<Contract> getBySellerId(int id);
}
