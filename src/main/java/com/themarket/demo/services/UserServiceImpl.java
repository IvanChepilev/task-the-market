package com.themarket.demo.services;

import com.themarket.demo.models.Contract;
import com.themarket.demo.models.User;
import com.themarket.demo.repositories.contracts.UserRepository;
import com.themarket.demo.services.contracts.UserService;
import org.javamoney.moneta.Money;
import org.javamoney.moneta.convert.ecb.ECBHistoricRateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.NumberValue;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void update(User user) {
        repository.update(user);
    }

    @Override
    public User setSellerAccount(User seller, Contract contract) {
        User userToUpdate = new User();
        userToUpdate.setId(seller.getId());
        userToUpdate.setUsername(seller.getUsername());
        userToUpdate.setAccount(seller.getAccount() + contract.getPrice());
        return userToUpdate;
    }

    @Override
    public User setBuyerAccount(User buyer, Contract contract) {
        User userToUpdate = new User();
        userToUpdate.setId(buyer.getId());
        userToUpdate.setUsername(buyer.getUsername());
        userToUpdate.setAccount(buyer.getAccount() - contract.getPrice());
        return userToUpdate;
    }



}
