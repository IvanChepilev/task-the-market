package com.themarket.demo.services;

import com.themarket.demo.exeptions.EntityNotFoundException;
import com.themarket.demo.models.Contract;
import com.themarket.demo.models.DTOs.ContractClosingDTO;
import com.themarket.demo.models.DTOs.ContractDTO;
import com.themarket.demo.models.Item;
import com.themarket.demo.models.Status;
import com.themarket.demo.models.User;
import com.themarket.demo.repositories.contracts.ContractRepository;
import com.themarket.demo.services.contracts.ContractService;
import com.themarket.demo.services.contracts.ItemService;
import com.themarket.demo.services.contracts.UserService;
import com.themarket.demo.utils.mappers.ContractMapper;
import org.javamoney.moneta.convert.ecb.ECBHistoricRateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.MonetaryConversions;
import java.util.List;
import java.util.Optional;

@Service
public class ContractServiceImpl implements ContractService {

    private final ContractRepository repository;
    private final ContractMapper mapper;
    private final ItemService itemService;
    private final UserService userService;

    @Autowired
    public ContractServiceImpl(ContractRepository repository, ContractMapper mapper, ItemService itemService, UserService userService) {
        this.repository = repository;
        this.mapper = mapper;
        this.itemService = itemService;
        this.userService = userService;
    }

    @Override
    public Contract create(ContractDTO contractDTO) {
        try {
            Item item = itemService.getById(contractDTO.getItemId());
            Contract newContract = mapper.fromDTO(item, contractDTO);
            return repository.create(newContract);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Item", contractDTO.getItemId());
        }
    }


    @Override
    public Contract update(ContractDTO contractDTO) {
        try {
            Contract contractToUpdate = repository.getById(contractDTO.getItemId());
            contractToUpdate = mapper.fromDTO(contractToUpdate, contractDTO);
            return repository.update(contractToUpdate);
        } catch (Exception e) {
            throw new EntityNotFoundException("Item", contractDTO.getItemId());
        }
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public Contract getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Contract getByItemId(int id) {
        return repository.getByItemId(id);
    }

    @Override
    public List<Contract> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Contract> getAllActiveContracts() {
        return repository.getAllActiveContracts();
    }

    @Override
    public void closing(ContractClosingDTO contractClosingDTO) {
        Contract contractForClosing = getByItemId(contractClosingDTO.getItemId());
        User buyer = userService.getById(contractClosingDTO.getBuyerId());
        User seller = userService.getById(contractForClosing.getSeller().getId());
        Item item = itemService.getById(contractForClosing.getItemId().getId());
        if (!checkAccount(contractForClosing, buyer)) {
            throw new UnsupportedOperationException("Not enough account");
        }
        itemService.update(itemService.setItemOwner(item, buyer));
        userService.update(userService.setSellerAccount(seller, contractForClosing));
        userService.update(userService.setBuyerAccount(buyer, contractForClosing));
        contractForClosing = setContractForClosing(contractForClosing, buyer);
        repository.update(contractForClosing);

    }

    public boolean checkAccount(Contract contract, User buyer) {
        boolean isEnough = true;
        if (contract.getPrice() > buyer.getAccount()) {
            return false;
        }
        return isEnough;
    }

    public Contract setContractForClosing(Contract contract, User user) {
        Contract closedContract = new Contract();
        Status status = new Status();
        status.setId(2);
        status.setStatusName("false");
        closedContract.setId(contract.getId());
        closedContract.setSeller(contract.getSeller());
        closedContract.setPrice(contract.getPrice());
        closedContract.setItemId(contract.getItemId());
        closedContract.setStatus(status);
        closedContract.setBuyer(user);
        return closedContract;
    }

    @Override
    public List<Contract> search(Optional<String> parameter, Optional<Integer> id) {
        if (parameter.isEmpty() && id.isEmpty()) {
            return repository.getAll();
        }
        if (parameter.isEmpty()) {
            throw new UnsupportedOperationException("Parameter can not be empty");
        }
        if (id.isEmpty()) {
            throw new UnsupportedOperationException("Id can not be empty");
        }
        return repository.search(parameter, id);
    }

    @Override
    public List<Contract> getBySellerId(int id) {
        return repository.getBySellerId(id);
    }
}
