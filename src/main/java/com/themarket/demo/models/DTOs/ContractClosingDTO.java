package com.themarket.demo.models.DTOs;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class ContractClosingDTO {

    @Column(name= "item_id")
    @NotNull(message = "Price can't be empty")
    private int itemId;

    @Column(name= "buyer_id")
    @NotNull(message = "Buyer Id can't be empty")
    private int buyerId;

    public ContractClosingDTO() {
    }

    public ContractClosingDTO(int itemId, int buyerId) {
        this.itemId = itemId;
        this.buyerId = buyerId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }
}
