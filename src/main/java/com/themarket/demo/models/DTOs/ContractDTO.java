package com.themarket.demo.models.DTOs;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class ContractDTO {

    @Column(name= "item_id")
    @NotNull(message = "Price can't be empty")
    private int itemId;

    @Column(name= "price")
    @NotNull(message = "Price can't be empty")
    private double price;


    public ContractDTO() {
    }

    public ContractDTO(int itemId, double price) {
        this.itemId = itemId;
        this.price = price;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
