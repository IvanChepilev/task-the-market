package com.themarket.demo.models.DTOs;

import com.themarket.demo.models.User;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

public class ItemDTO {

    @Column(name = "item_name")
    @NotNull(message = "Item name can't be empty")
    private String name;


    @Column(name = "owner_id")
    @NotNull(message = "Item owner ID can't be empty")
    private int ownerId;

    public ItemDTO() {
    }

    public ItemDTO(String name, int ownerId) {
        this.name = name;
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }
}
