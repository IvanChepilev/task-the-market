package com.themarket.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EfbetApplication {

    public static void main(String[] args) {
        SpringApplication.run(EfbetApplication.class, args);
    }

}
