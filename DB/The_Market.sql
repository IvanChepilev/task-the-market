-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.68-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for the_market
CREATE DATABASE IF NOT EXISTS `the_market` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `the_market`;

-- Dumping structure for table the_market.contracts
CREATE TABLE IF NOT EXISTS `contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `price` double DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contracts_users_id_fk` (`buyer_id`),
  KEY `contracts_users_id_fk_2` (`seller_id`),
  KEY `contracts_contracts_statuses_id_fk` (`status_id`),
  KEY `contracts_items_id_fk` (`item_id`),
  CONSTRAINT `contracts_contracts_statuses_id_fk` FOREIGN KEY (`status_id`) REFERENCES `contracts_statuses` (`id`),
  CONSTRAINT `contracts_items_id_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `contracts_users_id_fk` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `contracts_users_id_fk_2` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table the_market.contracts: ~3 rows (approximately)
/*!40000 ALTER TABLE `contracts` DISABLE KEYS */;
INSERT INTO `contracts` (`id`, `seller_id`, `buyer_id`, `item_id`, `price`, `status_id`) VALUES
	(1, 1, 2, 1, 50, 1),
	(2, 1, 2, 2, 40, 2),
	(3, 1, 2, 1, 60, 2);
/*!40000 ALTER TABLE `contracts` ENABLE KEYS */;

-- Dumping structure for table the_market.contracts_statuses
CREATE TABLE IF NOT EXISTS `contracts_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table the_market.contracts_statuses: ~2 rows (approximately)
/*!40000 ALTER TABLE `contracts_statuses` DISABLE KEYS */;
INSERT INTO `contracts_statuses` (`id`, `status_name`) VALUES
	(1, 'true'),
	(2, 'false');
/*!40000 ALTER TABLE `contracts_statuses` ENABLE KEYS */;

-- Dumping structure for table the_market.items
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(20) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `items_users_id_fk` (`owner_id`),
  CONSTRAINT `items_users_id_fk` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table the_market.items: ~4 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `item_name`, `owner_id`) VALUES
	(1, 'Item1', 1),
	(2, 'Item2', 2),
	(3, 'Item3', 2),
	(4, 'Item4', 1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table the_market.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `account` double(11,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table the_market.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `account`) VALUES
	(1, 'User1', 280),
	(2, 'User2', 20);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
